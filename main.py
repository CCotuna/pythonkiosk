import json
import random
from datetime import datetime
import os

with open("menu.txt", "r") as f:
    menu = json.load(f)

cart = {}
products = {}
total_price = 0
order_number = 0

def display_menu():
    print("\nWelcome!")
    for item in menu:
        print(f"ID meniu: {item['IDmeniu']} - {item['MeniuGeneral']} - ID product: ({item['ID']}) - {item['Tip']} -> {item['Pret']}RON")

def display_one_section():
    menu_list = {}
    print("\nThis is our main menu!")
    for item in menu:
        menu_list.update({item['IDmeniu']:item['MeniuGeneral']})

    for item in menu_list:
        print(f"ID: {item} - {menu_list[item]}")

    choice = int(input("\nEnter section's number: [Ex: 1 - Meniu, 2 - Garnitura...]"))

    for item in menu:
        if choice == item['IDmeniu']:
            print(f"ID:{item['ID']} - Tip {item['MeniuGeneral']}: {item['Tip']} {item['Pret']}RON")
        else:
            continue

def display_current_cart():
    print("\nThis is your current cart.\n Product_name // Product_quantity\n")

    for key in cart:
        print(f"{cart[key]} - {products[key]}")


def add_to_cart():
    global total_price

    menu_list = {}
    print("\nThis is our main menu!")
    for item in menu:
        menu_list.update({item['IDmeniu']: item['MeniuGeneral']})
    for item in menu_list:
        print(f"ID: {item} - {menu_list[item]}")

    menu_id = int(input("\nWhat would you like to order?"))

    for item in menu:
        if menu_id == item['IDmeniu']:
            print(f"ID:{item['ID']} - Tip {item['MeniuGeneral']}: {item['Tip']} {item['Pret']}RON")
        else:
            continue

    item_id = int(input("\nChoose a product from the list. "))
    found_item = False
    for item in menu:
        if menu_id == item['IDmeniu']:
            if item_id == item['ID']:
                found_item = True
                product_name = item['Tip']
                item_price = item['Pret']
                break
        else:
            continue
    if not found_item:
        print("Sorry, this item doesn't belong to our menu")
        return
    item_quantity = int(input("How many would you like to order?"))
    if item_id in cart:
        cart[item_id] = product_name
        products[item_id] += item_quantity
    else:
        cart[item_id] = product_name
        products[item_id] = item_quantity

    total_price += item_price * item_quantity
    print(f"The product was added to your cart. Total price: {total_price}RON")
    choice_to_continue = int(input("If you want to add another product type '1' else type '0'. "))
    if choice_to_continue == 1:
        add_to_cart()
    else:
        return

def remove_from_cart():
    if not bool(cart):
        print("Your cart is empty. Add some products!")
    else:
        global total_price
        print("\nThis is your current cart.\n")
        for key in cart:
            print(f"Product: {cart[key]} - ID: {key} - Quantity: {products[key]}")
        remove_item = int(input("\nWould you like to remove completely a product? If yes type ' 1 ' else type ' 0 ' =>"))
        if remove_item == 1:
            remove_option_by_id = int(input("What would you like to remove from the cart? ! Select based on ID. !"))
            del cart[remove_option_by_id]
            del products[remove_option_by_id]
        else:
            remove_option_by_id = int(input("What would you like to remove from the cart? ! Select based on ID. !"))

            if remove_option_by_id not in cart:
                print("Sorry, that item it's not in your cart.")
                return

            remove_option_quantity = int(input(f"\nHow many items of {cart[remove_option_by_id]} would you like to remove?"))
            if remove_option_quantity < 0:
                print("You must enter a positive number: > 0")
            elif remove_option_quantity > products[remove_option_by_id]:
                print("You don't have that quantity in your cart")

            for item in menu:
                if item['ID'] == remove_option_by_id:
                    item_price = item['Pret']
                    break
            products[remove_option_by_id] -= remove_option_quantity
            total_price -= item_price * remove_option_quantity
            if products[remove_option_by_id] == 0:
                del cart[remove_option_by_id]
                del products[remove_option_by_id]

            choice_to_continue = int(input("If you want to remove another product type '1' else type '0'. "))
            if choice_to_continue == 1:
                remove_from_cart()
            else:
                return

def modify_quantity():
    print("\nThis is your current cart.\n")
    for key in cart:
        print(f"Product: {cart[key]} - ID: {key} - Quantity: {products[key]}")
    loop = 1
    while loop == 1:
        modifier = int(input("\nWhose product would you like to change the quantity? Choose based on ID's product. " ))
        modifier_quantity = int(input("Choose the new quantity. "))
        if modifier_quantity == 0:
            del cart[modifier]
            del products[modifier]
            loop = int(input("Do you want to remove another product? If YES type '1', else type any other number different from '1'. "))
        else:
            products[modifier] = modifier_quantity
            loop = int(input("Do you want to change the quantity for another product? If yes type '1', else type any other number different from '1'. "))

def take_order():
    global order_number
    global total_price
    discount_code = 12345

    order_number = getNextOrderNumber()
    voucher_option = int(input("If you want to insert a voucher type 1 else type 0: "))
    if voucher_option == 1:
        voucher = int(input("Enter discount code for 10% off "))
        if voucher is None:
            print("Please insert a valid voucher.")
        elif not isinstance(voucher, int):
            print("The voucher is not valid.")
        elif voucher == discount_code:
            total_price *= 0.9
            print(f"Total price after applying the voucher: {total_price}")
    close = int(input("If you want to cancel the order type 1 else type 0: "))
    if close == 1:
        closeOrder = int(input("You sure you really wanna exit? If yes type 1 else type 0: "))
        if closeOrder == 1:
            return
    elif close == 0:
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

        with open(f'{order_number}', 'w') as file:
            orderNumber = random.randint(1, 60)
            file.write("\n****** Welcome ******")
            file.write(f"\nORDER NUMBER: {orderNumber}")
            file.write(f"\nOrder Date and Time: {dt_string}")
            file.write("\n\nThis is your order.")
            file.write("\n * * * KFC DT ORADEA * * *  ")
            restaurant = random.randint(1,200)
            file.write(f"\n* * * RESTAURANT : {restaurant} * * *")
            file.write("\n____________________________\n")
            for key in cart:
                file.write(f"\n{cart[key]} - {products[key]} x {menu[key]['Pret']} RON")
                # print(menu[key]['Pret'])
            file.write("\n____________________________")
            file.write(f"\nTotal price: {total_price} RON.")

            log_data = {"order number": order_number, "timestamp": str(datetime.now())}
            with open("order_log.json", "a") as log_file:
                log_file.write(json.dumps(log_data) + "\n")
def clear_cart():
    order_number = 0
    global total_price
    total_price = 0
    cart.clear()
    products.clear()

def checkForExistingOrders():
    global order_number

    files = os.listdir()
    order_nums = []
    for file in files:
        if file.isdigit():
            order_nums.append(int(file))
    if order_nums:
        order_number = max(order_nums)

def getNextOrderNumber():
    files = os.listdir()
    order_nums = []
    for file in files:
        if file.isdigit():
            order_nums.append(int(file))
    if order_nums:
        return max(order_nums) + 1
    else:
        return 1

checkForExistingOrders()

stillAdding = 1
while stillAdding == 1:
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("date and time =", dt_string)

    print("\n****************  Welcome to KFC! ***************")
    print("\nWhat would you like to do?")
    print("""
    0. Exit
    1. Display menu
    2. Display specific section
    3. Add to cart
    4. Remove from cart
    5. Display current cart
    6. Change products' quantity
    7. Place order
    """)
    user_choice = input("Choose an option. It must be between [0, 6]. Anything else is invalid.")
    if user_choice == "0":
        break
    elif user_choice == "1":
        display_menu()
    elif user_choice == "2":
        display_one_section()
    elif user_choice == "3":
        add_to_cart()
    elif user_choice == "4":
        remove_from_cart()
    elif user_choice == "5":
        display_current_cart()
    elif user_choice == "6":
        modify_quantity()
    elif user_choice == "7":
        order_number += 1
        take_order()
        clear_cart()
        stillAdding = int(input("Do you want to take another order? If YES type 1, else type any other number different from '1'[Ex: 0]."))
    else:
        print("Invalid choice. Use a number between 0 - 7")